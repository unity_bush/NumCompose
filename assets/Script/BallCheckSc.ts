import { Msger } from "./Msger";
import BallSc from "./BallSc";
import { GlobleValue, GameState } from "./ComClass";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class BallCheckSc extends cc.Component {

    @property(cc.Node)
    ballLayer: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        Msger.on(Msger.on_add_score, this.on_add_score, this)
    }
    private checkTimer = 0;
    private timerspan = 0.3;
    private on_add_score() {
        this.checkTimer = this.timerspan;
    }
    start() {

    }

    update(dt) {
        if (GlobleValue.gameState != GameState.Runing) {
            return;
        }
        this.checkTimer -= dt;
        if (this.checkTimer <= 0) {
            let array: cc.Node[] = [];
            let arrayBao: cc.Node[] = [];
            for (let i = this.ballLayer.childrenCount - 1; i >= 0; i--) {
                let ball1 = this.ballLayer.children[i];
                if (arrayBao.indexOf(ball1) >= 0 || array.indexOf(ball1) >= 0) {
                    continue;
                }
                for (let j = this.ballLayer.childrenCount - 1; j >= 0; j--) {
                    let ball2 = this.ballLayer.children[j];
                    if (arrayBao.indexOf(ball2) >= 0 || array.indexOf(ball2) >= 0) {
                        continue;
                    }
                    if (ball1 == ball2) {
                        continue;
                    }
                    let d = this.getDistance(ball1.x, ball1.y, ball2.x, ball2.y);
                    if (d < (ball1.width + ball2.width) / 2) {
                        if (ball1.getComponent(BallSc).size == ball2.getComponent(BallSc).size || ball2.getComponent(BallSc).ballType == 1) {
                            array.push(ball1);
                            arrayBao.push(ball2);
                            ball1.getComponent(BallSc).addSize(ball2);

                        }
                    }
                }
                for (let i = 0; i < arrayBao.length; i++) {
                    let ball2 = arrayBao[i];
                    if (ball2.getComponent(BallSc).ballType == 1) {//特殊球
                        if (ball2.getComponent(BallSc).haveGiftIcon) {
                            Msger.emit(Msger.on_ui_prop_gift);
                        }
                    }
                    Msger.emit(Msger.on_game_remove_ball, ball2);
                    Msger.emit(Msger.on_add_score, 20);
                }
            }
            this.checkTimer = this.timerspan / 3;
        }
    }

    private getDistance(x1, y1, x2, y2): number {
        return Math.floor(Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
    }
}
