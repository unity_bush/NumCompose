import { GameData } from "./ComClass";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class EffectBaoSc extends cc.Component {


    // LIFE-CYCLE CALLBACKS:
    private static baoFrames: cc.SpriteFrame[] = null;
    onLoad() {
        this.node.rotation = Math.random() * 360;
        this.init();
    }
    private index: number = 0;
    async init() {
        if (EffectBaoSc.baoFrames == null) {
            EffectBaoSc.baoFrames = await GameData.loadTextrueDirSync("eff/bao");
        }
    }
    private timespan = 0;
    update(dt) {
        if (EffectBaoSc.baoFrames != null) {
            this.timespan += dt;
            if (this.timespan > 0.1) {
                this.timespan = 0;
                this.index += 1;
                if (this.index >= EffectBaoSc.baoFrames.length) {
                    this.node.destroy();
                } else {
                    this.getComponent(cc.Sprite).spriteFrame = EffectBaoSc.baoFrames[this.index];
                }
            }
        }
    }
}
