import { Msger } from "./Msger";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class AudioSystemSc extends cc.Component {

    @property({type:cc.AudioClip})
    ballSounds: cc.AudioClip[] = [];
    @property({type:cc.AudioClip})
    clickSound: cc.AudioClip = null;
    @property({type:cc.AudioClip})
    levelupSound: cc.AudioClip = null;
    @property({type:cc.AudioClip})
    overSound: cc.AudioClip = null;
    @property({type:cc.AudioClip})
    startSound: cc.AudioClip = null;
    @property({type:cc.AudioClip})
    takeGemSound: cc.AudioClip = null;
    @property({type:cc.AudioClip})
    wannengSound: cc.AudioClip = null;
    @property({type:cc.AudioClip})
    goodSound: cc.AudioClip = null;
    @property({type:cc.AudioClip})
    niceSound: cc.AudioClip = null;
    @property({type:cc.AudioClip})
    piSound: cc.AudioClip = null;

    public static isMute = false;
    /* @property(cc.Label)
    label: cc.Label = null; */

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        Msger.on(Msger.on_sound_mute_change, this.on_sound_mute_change, this);
        Msger.on(Msger.on_sound_play_ball, this.on_sound_play_ball, this);
        Msger.on(Msger.on_sound_play, this.on_sound_play, this);
    }

    //start () {   }

    // update (dt) {}
    private on_sound_mute_change() {
        AudioSystemSc.isMute = !AudioSystemSc.isMute;
    }
    private on_sound_play_ball(size) {
        if (AudioSystemSc.isMute) return;
        let index = size - 1;
        if (index < 0) {
            index = 0;
        }
        if (index > this.ballSounds.length) {
            index = this.ballSounds.length - 1;
        }
        cc.audioEngine.play(this.ballSounds[index], false, 1);
    }
    private on_sound_play(type) {
        if (AudioSystemSc.isMute) return;
        switch (type) {
            case 1:
                cc.audioEngine.play(this.clickSound, false, 1);
                break;
            case 2:
                cc.audioEngine.play(this.levelupSound, false, 1);
                break;
            case 3:
                cc.audioEngine.play(this.overSound, false, 1);
                break;
            case 4:
                cc.audioEngine.play(this.startSound, false, 1);
                break;
            case 5:
                cc.audioEngine.play(this.takeGemSound, false, 1);
                break;
            case 6:
                cc.audioEngine.play(this.wannengSound, false, 1);
                break;
            case 7:
                cc.audioEngine.play(this.goodSound, false, 1);
                break;
            case 8:
                cc.audioEngine.play(this.niceSound, false, 1);
                break;
            case 9:
                cc.audioEngine.play(this.piSound, false, 1);
                break;
        }
    }
}
