import { GameLocalStorage, GlobleValue } from "./ComClass";
import MainSc from "./MainSc";
import { Msger } from "./Msger";
import BallSc from "./BallSc";
import { ComLogic } from "./WxPlatfromLogic";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class PropHandleSc extends cc.Component {

    @property(cc.Label)
    wanneng: cc.Label = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    update(dt) {
        if (this.wanneng && GameLocalStorage.gameData) {
            this.wanneng.string = GameLocalStorage.gameData.wannengprop.toString();
        }
    }

    on_ui_click_prop(e) {
        Msger.emit(Msger.on_sound_play, 1);
        if (GameLocalStorage.gameData.wannengprop <= 0) {
            /* if (GameLocalStorage.gameData.wannenggetcount < 3) {
                let str = "道具不够了\n求助一下好友吧!";
                if (GlobleValue.ipSwitch || GlobleValue.gameSwitch ) {
                    str = "道具不够了\n看视频领取";
                }
                Msger.emit(Msger.on_ui_show_message, str, this.shareGetProp, this, () => { }, this);
            } else {
                Msger.emit(Msger.on_ui_show_message, "每局最多可获得3个万能泡泡");
            } */
        } else {
            if (!this.getComponent(MainSc).nextball) return;
            if (this.getComponent(MainSc).nextball.getComponent(BallSc).ballType == 1) {
                //对于已经是万能的当前球不做处理
                return;
            }
            GameLocalStorage.gameData.wannengprop -= 1;
            Msger.emit(Msger.on_ui_click_prop, 1);
            let nextballsc = this.getComponent(MainSc).nextball.getComponent(BallSc)
            nextballsc.setBallType(1);
        }
    }
    private async shareGetProp() {
        let ret = await ComLogic.shareGoto("prop0", true);
        if (ret) {
            GameLocalStorage.gameData.wannengprop += 1;
            GameLocalStorage.gameData.wannenggetcount += 1;
        }
    }
}
