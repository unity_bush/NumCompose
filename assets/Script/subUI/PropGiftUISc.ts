import { ComLogic } from "../WxPlatfromLogic";
import { GameLocalStorage, GlobleValue } from "../ComClass";
import { Msger } from "../Msger";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class PropGiftUISc extends cc.Component {

    @property(cc.Node)
    afterPanel: cc.Node = null;

    @property(cc.Node)
    animatPanel: cc.Node = null;

    @property(cc.Label)
    btnLabel: cc.Label = null;
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.afterPanel.active = false;
        if (GlobleValue.gameSwitch) {
            //this.btnLabel.string = "看视频领取";
        }
    }

    animatComplated() {
        this.afterPanel.active = true;
    }
    getAnimatComplate(){
        this.onclick_close();
    }
    // update (dt) {}
    async onclick_get() {
        Msger.emit(Msger.on_sound_play,1);
        let ret = await ComLogic.shareGoto("prop1", true);
        if (ret) {
            GameLocalStorage.gameData.wannengprop += 1;
            GameLocalStorage.gameData.gotPropSum += 1;
            this.afterPanel.active = false;
            this.animatPanel.active = false;
            this.getComponent(cc.Animation).play("propgifteffect");
        }
    }
    onclick_close() {
        Msger.emit(Msger.on_sound_play,1);
        this.node.destroy();
    }
}