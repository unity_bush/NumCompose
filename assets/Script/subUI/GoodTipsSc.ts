import { Msger } from "../Msger";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class GoodTipsSc extends cc.Component {

    @property(cc.Prefab)
    good: cc.Prefab = null;

    @property(cc.Prefab)
    prefect: cc.Prefab = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        Msger.on(Msger.on_add_score, this.on_add_score, this)
    }
    private combo = 0;
    private on_add_score() {
        if (this.socreTimer < 0.2) {
            this.combo += 1;
        }
        this.socreTimer = 0;
    }
    private socreTimer = 0;
    update(dt) {
        this.socreTimer += dt;
        if (this.socreTimer >= 0.2 && this.combo >= 2) {
            let obj: cc.Node;
            if (this.combo == 2) {
                obj = cc.instantiate(this.good);
                Msger.emit(Msger.on_sound_play, 7);
            }
            if (this.combo >= 3) {
                obj = cc.instantiate(this.prefect);
                Msger.emit(Msger.on_sound_play, 8);
            }
            this.combo = 0;
            if (obj) {
                obj.x = -640;
                obj.runAction(cc.sequence(cc.delayTime(2), cc.callFunc(obj.destroy, obj)));
                Msger.emit(Msger.on_game_add_effect, { obj: obj });
            }
        }
    }
}
