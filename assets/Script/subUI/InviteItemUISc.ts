import { GameLocalStorage } from "../ComClass";
import { ComLogic } from "../WxPlatfromLogic";
import { Msger } from "../Msger";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class InviteItemUISc extends cc.Component {

    @property(cc.Label)
    gem: cc.Label = null;
    @property(cc.Node)
    btnGet: cc.Node = null;
    @property(cc.Node)
    btnShare: cc.Node = null;
    @property(cc.Node)
    label: cc.Node = null;
    @property(cc.Node)
    labelNew: cc.Node = null;
    @property(cc.Node)
    addGem: cc.Node = null;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    private gemCount = 0;
    public weight: number = 0;
    private isnew;
    // update (dt) {}
    setData(gem, state, isnew) {
        this.isnew = isnew;
        this.btnGet.active = state == 1;
        this.btnShare.active = state == 2;
        this.gemCount = gem;
        this.gem.string = gem.toString();
        this.label.active = !isnew;
        this.labelNew.active = isnew;
        this.addGem.active = isnew;
        if (this.btnGet.active) {
            if (isnew) {
                this.weight = 100;
            } else {
                this.weight = 10;
            }
        }
    }
    onclick_get() {
        Msger.emit(Msger.on_sound_play,1);
        let count = this.gemCount;
        if (this.isnew) {
            count += 100;
        }
        Msger.emit(Msger.on_take_some_gem, count, "invite", false);
        Msger.emit(Msger.on_invite_get, this.isnew);
        this.btnGet.active =
            this.btnShare.active = false;
        //this.node.destroy();
    }
    onclick_share() {
        Msger.emit(Msger.on_sound_play,1);
        ComLogic.shareto("invite", false);
    }
}
