import { Msger } from "../Msger";
import { ComLogic } from "../WxPlatfromLogic";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class VideoAwardButtonSc extends cc.Component {

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.node.active = false;
        Msger.on(Msger.on_network_login_completed, this.on_network_login_completed, this);
    }

    start() {

    }
    private count = 0;
    async onclick_self() {
        let ret = await ComLogic.createRewardedVideoAd();
        if (ret) {
            Msger.emit(Msger.on_take_some_gem, 50, "video", false);
            this.count -= 1;
            localStorage.setItem("Po2048VideoAwardButton", this.count.toString());
            if (this.count <= 0) {
                this.node.active = false;
            }
        }
    }
    on_network_login_completed() {
        Msger.off(Msger.on_network_login_completed, this.on_network_login_completed, this);
        this.node.active = true;
        let countstr = localStorage.getItem("Po2048VideoAwardButton");
        if (!!countstr) {
            this.count = parseInt(countstr);
        } else {
            this.count = 3;
        }
        if (this.count <= 0) {
            this.node.active = false;
        }
    }
    // update (dt) {}
}
