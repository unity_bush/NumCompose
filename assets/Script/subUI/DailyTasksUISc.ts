import { GlobleValue, GameState } from "../ComClass";
import { Msger } from "../Msger";
const {ccclass, property} = cc._decorator;

@ccclass
export default class DailyTasksUISc extends cc.Component {

    @property(cc.Node)
    items: cc.Node = null;


    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        GlobleValue.gameState = GameState.SubUI;
    }

    // update (dt) {}
    onclick_close(){
        Msger.emit(Msger.on_sound_play,1);
        this.node.destroy();
    }
}
