import { Msger } from "../Msger";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class DockIconSc extends cc.Component {

    @property(cc.Node)
    dot: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    haveDot:boolean = true;
    start() {

    }
    private data: { appId, dot, icon, img, path, type } = null
    update (dt) {
        this.dot.active = this.haveDot;
    }
    initData(data,width,height) {
        this.data = data;
        let sprite = this.getComponent(cc.Sprite);
        sprite.node.width = width;
        sprite.node.height = height;
        this.haveDot = this.dot.active = this.data.dot == 1;
        if(this.data.icon){
            cc.loader.load({
                url: this.data.icon, type: 'png'
            }, (err, texture) => {
                if (err) console.error(err);
                sprite.spriteFrame = new cc.SpriteFrame(texture);
                sprite.node.width = width;
                sprite.node.height = height;
            });
        }
    }
    onclick_self(){
        Msger.emit(Msger.on_sound_play,1);
        if(this.data){
            window.wxplatform.navigateToMiniProgram(this.data.appId, this.data.path, this.data.img, this.data.type == 1);
        }
    }
}
