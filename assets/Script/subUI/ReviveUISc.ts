import { GlobleValue, GameState, GameLocalStorage, Gamelanguage } from "../ComClass";
import { Msger } from "../Msger";
import { ComLogic } from "../WxPlatfromLogic";


const { ccclass, property } = cc._decorator;

@ccclass
export default class ReviveUISc extends cc.Component {

    @property(cc.Sprite)
    ring: cc.Sprite = null;
    @property(cc.Label)
    score: cc.Label = null;
    @property(cc.Label)
    btnLabel: cc.Label = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        let big = cc.scaleTo(0.4, 1.1);
        let small = cc.scaleTo(0.4, 1);
        let seq = cc.sequence(big, small);
        let rep = cc.repeatForever(seq);
        this.ring.node.parent.runAction(rep);
        window.wxplatform.createBannerAd();
        if (GlobleValue.gameSwitch) {
            this.btnLabel.string = "看视频复活";
        }        
    }

    start() {
        GlobleValue.gameState = GameState.SubUI;
        this.score.string = GameLocalStorage.gameData.currentScore.toString();
    }

    update(dt) {
/*         if (!this.holdbtn) {
            this.ring.fillRange -= dt * 0.1;
            if (this.ring.fillRange < 0) {
                this.onclick_close();
            }
        } */
    }
    async onclick_revive() {
        Msger.emit(Msger.on_sound_play, 1);
        if(GameLocalStorage.gameData.gem >= 100){
            GameLocalStorage.gameData.gem -= 100;
            GameLocalStorage.Save();
            this.ring.node.parent.stopAllActions();
            Msger.emit(Msger.on_game_revive);
            this.node.destroy();
        }else{
            Msger.emit(Msger.on_ui_show_message,Gamelanguage.getText(Gamelanguage.tip_Gemnotenough));
        }
        return;
        let ret = await ComLogic.shareGoto("revive", true);
        if (ret) {
            this.ring.node.parent.stopAllActions();
            Msger.emit(Msger.on_game_revive);
            this.node.destroy();
        }
    }
    onclick_close() {
        GameLocalStorage.gameData.inrevive = null;
        GameLocalStorage.Save();
        this.ring.node.parent.stopAllActions();
        GameLocalStorage.gameData.reviveTimes = 0;
        Msger.emit(Msger.on_game_die);
        this.node.destroy();
    }
}
