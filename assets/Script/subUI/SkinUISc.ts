import { GlobleValue, GameState, GameData, GameLocalStorage } from "../ComClass";
import SkinUiItemSc from "./SkinUiItemSc";
import { Msger } from "../Msger";
const { ccclass, property } = cc._decorator;

@ccclass
export default class SkinUISc extends cc.Component {

    @property(cc.Node)
    items: cc.Node = null;

    @property(cc.Prefab)
    itemPrefab: cc.Prefab = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        window.wxplatform.createBannerAd();
        for (let i = 0; i < 100; i++) {
            let data = GameData.themeConfig[i];
            if (data) {
                let item = cc.instantiate(this.itemPrefab);
                item.getComponent(SkinUiItemSc).setSkinData(i, data);
                this.items.addChild(item);
                if (i == GameLocalStorage.gameData.themekey) {
                    item.getComponent(SkinUiItemSc).isSelected = true;
                }
            }
        }
    }

    start() {
        GlobleValue.gameState = GameState.SubUI;
    }

    // update (dt) {}
    onclick_close() {
        Msger.emit(Msger.on_sound_play,1);
        if(GlobleValue.gameState == GameState.Start){
            window.wxplatform.bannerAdHide();
        }
        this.node.destroy();
    }
}
