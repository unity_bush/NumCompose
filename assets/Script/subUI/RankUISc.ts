import { GlobleValue, GameState } from "../ComClass";
import { Msger } from "../Msger";
const { ccclass, property } = cc._decorator;

@ccclass
export default class RankUISc extends cc.Component {
    /* 
        @property(cc.Node)
        items: cc.Node = null;
     */

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        Msger.emit(Msger.on_subcontext_to_rank);
        window.wxplatform.createBannerAd();
    }

    start() {
        //GlobleValue.gameState = GameState.SubUI;
    }

    // update (dt) {}
    onclick_close() {
        Msger.emit(Msger.on_sound_play,1);
        if (GlobleValue.gameState == GameState.Over) {
            Msger.emit(Msger.on_subcontext_to_over);
        } else {
            Msger.emit(Msger.on_subcontext_to_close);
        }
        this.node.destroy();
    }
}
