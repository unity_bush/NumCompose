import { AchievementData, GameLocalStorage } from "../ComClass";
import { ComLogic } from "../WxPlatfromLogic";
import { Msger } from "../Msger";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class AchievementItemUISc extends cc.Component {

    @property(cc.Label)
    aName: cc.Label = null;
    @property(cc.Label)
    aTips: cc.Label = null;
    @property(cc.Label)
    aCount: cc.Label = null;
    @property(cc.Label)
    aGem: cc.Label = null;

    @property(cc.ProgressBar)
    progressBar: cc.ProgressBar = null;
    @property(cc.Node)
    aBtnShare: cc.Node = null;
    @property(cc.Node)
    aBtnGet: cc.Node = null;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    onclick_get() {
        Msger.emit(Msger.on_sound_play, 1);
        Msger.emit(Msger.on_take_some_gem, this.data.aGem, "achieve1", true);
        AchievementData.haveAchievement.push(this.data.aID);
        AchievementData.Save();
        this.aBtnGet.active = false;
        this.aBtnShare.active = true;
    }
    onclick_share() {
        Msger.emit(Msger.on_sound_play, 1);
        ComLogic.shareto("achieve0", false);
    }
    private data: AchievementData.AchievementConfigItem;
    updateData(data: AchievementData.AchievementConfigItem) {
        switch (GameLocalStorage.gameData.language) {
            case "zh":
                this.aName.string = data.aNameZh;
                this.aTips.string = data.aTipZh;
                break;
            case "en":
                this.aName.string = data.aName;
                this.aTips.string = data.aTip;
                break;
        }

        this.aGem.string = data.aGem.toString();
        this.data = data;
        this.aBtnGet.active = this.aBtnShare.active = false;
        if (AchievementData.haveAchievement.indexOf(data.aID) >= 0) {
            //this.aBtnShare.active = true;
            this.aCount.string = data.aMaxCount + "/" + data.aMaxCount;
            this.progressBar.progress = 1;
        } else {
            let count = 0;
            switch (data.aType) {
                case 1://登录游戏
                    count = GameLocalStorage.gameData.playCount;
                    break;
                case 2://获得数字
                    count = GameLocalStorage.gameData.max;
                    break;
                case 3://得到分数
                    count = GameLocalStorage.gameData.bestScore;
                    break;
                case 4://皮肤数量
                    count = GameLocalStorage.gameData.gotThemes.length;
                    break;
                case 5://道具情况
                    count = GameLocalStorage.gameData.gotPropSum;
                    break;
                case 6://邀请好友数
                    count = GameLocalStorage.gameData.inviteCount;
                    break;
                case 7://邀请好友数
                    count = GameLocalStorage.gameData.bianbian;
                    break;
            }
            if (count > data.aMaxCount) {
                count = data.aMaxCount;
            }
            this.aCount.string = count + "/" + data.aMaxCount;
            this.progressBar.progress = count / data.aMaxCount;
            this.aBtnGet.active = this.progressBar.progress == 1;
        }
    }
}