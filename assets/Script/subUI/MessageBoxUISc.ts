import { Msger } from "../Msger";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class MessageBoxUISc extends cc.Component {
    @property(cc.Label)
    message: cc.Label = null;
    @property(cc.Node)
    btnClose: cc.Node = null;
    // LIFE-CYCLE CALLBACKS:
    onLoad() { }
    private agreecallback: Function;
    private closecallback: Function;
    private agreethisobj;
    private closethisobj;
    showUI(message, agreecallback, agreethisobj, closecallback = null, closethisobj = null) {
        this.message.string = message;
        this.agreecallback = agreecallback;
        this.closecallback = closecallback;
        this.agreethisobj = agreethisobj;
        this.closethisobj = closethisobj;
        if(!closecallback){
            this.btnClose.active = false;
        }
    }
    // update (dt) {}
    onclick_agree() {
        Msger.emit(Msger.on_sound_play,1);
        if (this.agreecallback) {
            this.agreecallback.call(this.agreethisobj);
        }
        this.removeself();
    }
    onclick_close() {
        Msger.emit(Msger.on_sound_play,1);
        if (this.closecallback) {
            this.closecallback.call(this.closethisobj);
        }
        this.removeself();
    }
    private removeself() {
        this.agreecallback = null;
        this.closecallback = null;
        this.agreethisobj = null;
        this.closethisobj = null;
        this.node.destroy();
    }
}
