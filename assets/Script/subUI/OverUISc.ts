import { Msger } from "../Msger";
import { GlobleValue, GameState, GameLocalStorage, GameData } from "../ComClass";
// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class OverUISc extends cc.Component {

    @property(cc.Label)
    score: cc.Label = null;

    @property(cc.Label)
    scoreBest: cc.Label = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        GlobleValue.gameState = GameState.Over;
        this.score.string = GameLocalStorage.gameData.currentScore.toString();
        this.scoreBest.string = GameLocalStorage.gameData.bestScore.toString();
        Msger.emit(Msger.on_subcontext_to_over);
        
        //清理游戏数据，避免错误的数据存在
        GameLocalStorage.gameData.gameSave = null;
        GameLocalStorage.gameData.currentScore = 0;
        GameLocalStorage.gameData.levelprogress = 0;
        GameLocalStorage.gameData.shotStep = 0;
        GameLocalStorage.gameData.levelprogressscore = 0;
        GameLocalStorage.gameData.wannengprop = 1;
        GameLocalStorage.gameData.reviveTimes = 1;
        Msger.emit(Msger.on_level_progress_zero);//顶部的进度分数计算
        GameLocalStorage.Save();
    }

    // update (dt) {}

    onclick_play_agin() {
        Msger.emit(Msger.on_sound_play,1);
        Msger.emit(Msger.on_game_reset_play);
        GlobleValue.gameState = GameState.Runing;
        this.onclick_close();
    }
    onclick_back_start() {
        Msger.emit(Msger.on_sound_play,1);
        GlobleValue.gameState = GameState.Start;
        Msger.emit(Msger.on_game_reset_play);
        Msger.emit(Msger.on_ui_click_home);
        this.onclick_close();
    }
    onclick_close() {
        Msger.emit(Msger.on_sound_play,1);
        Msger.emit(Msger.on_subcontext_to_close);
        this.node.destroy();
    }

    onclick_rank() {
        Msger.emit(Msger.on_sound_play,1);
        Msger.emit(Msger.on_ui_click_rank);
    }
}
