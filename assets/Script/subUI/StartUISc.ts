import { GlobleValue, GameState, GameLocalStorage, Gamelanguage } from "../ComClass";
import { Msger } from "../Msger";
import AudioSystemSc from "../AudioSystemSc";
import { ComLogic } from "../WxPlatfromLogic";
import DockIconSc from "./DockIconSc";

const { ccclass, property } = cc._decorator;

@ccclass
export default class StartUISc extends cc.Component {

    @property(cc.Node)
    hutuiGroup: cc.Node = null;
    @property(cc.Label)
    version: cc.Label = null;
    @property(cc.Label)
    bestScore: cc.Label = null;
    @property(cc.Label)
    gemCount: cc.Label = null;

    @property(cc.Node)
    soundOff: cc.Node = null;
    @property(cc.Node)
    soundOn: cc.Node = null;
    @property(cc.Prefab)
    dockIcon: cc.Prefab = null;

    @property(cc.Prefab)
    debugUI: cc.Prefab = null;

    onLoad() {
        Msger.on(Msger.on_network_login_completed, this.on_network_login_completed, this);
        /* if (ComLogic.dockArray) {
            this.on_network_login_completed();
        } */        
    }

    start() {
    }
    private dailyAwardTimer = 0;
    update(dt) {
        if (this.dailyAwardTimer > 0) {
            this.dailyAwardTimer -= dt;
            if (this.dailyAwardTimer <= 0) {
                //每日奖励
                let text = "每日奖励";
                switch (GameLocalStorage.gameData.language) {
                    case "zh":
                        text = "每日奖励";
                        break;
                    case "en":
                        text = "Daily reward";
                        break;
                }
                Msger.emit(Msger.on_take_some_gem, 50, "login", true, text);
            }
        }
        let mute = AudioSystemSc.isMute;//静音判断
        this.soundOff.active = mute;
        this.soundOn.active = !mute;
        if (this.login_completed) {
            this.gemCount.string = GameLocalStorage.gameData.gem.toString();
            this.bestScore.string = GameLocalStorage.gameData.bestScore.toString();
        }
    }
    private login_completed = false;
    on_network_login_completed() {
        GlobleValue.gameState = GameState.Start;
        //检查是不是同一天
        let gamedata = GameLocalStorage.gameData;
        if (gamedata.lastplayday != new Date().getDate()) {
            this.dailyAwardTimer = 2;
            //游戏次数增加
            GameLocalStorage.gameData.lastplayday = new Date().getDate();
            GameLocalStorage.gameData.playCount += 1;
            GameLocalStorage.Save();
        }

        Msger.off(Msger.on_network_login_completed, this.on_network_login_completed, this);
        if (this.hutuiGroup.childrenCount > 0) {
            return;
        }
        if (ComLogic.hutuiArray) {
            let hutuiArray = [];
            while (hutuiArray.length != ComLogic.hutuiArray.length) {
                let random = Math.floor(Math.random() * ComLogic.hutuiArray.length);
                let dataitem = ComLogic.hutuiArray[random];
                if (hutuiArray.indexOf(dataitem) < 0) {
                    hutuiArray.push(dataitem);
                }
            }
            for (let i = 0; i < 3 && i < hutuiArray.length; i++) {
                let icon = cc.instantiate(this.dockIcon);
                icon.getComponent(DockIconSc).initData(hutuiArray[i], 124, 124 * 1.08);
                this.hutuiGroup.addChild(icon);
                icon.getComponent(DockIconSc).haveDot = false;
            }
        }
        /* let icon = cc.instantiate(this.dockIcon);
        icon.getComponent(DockIconSc).initData(ComLogic.moreGame, 124, 124 * 1.08);
        this.hutuiGroup.addChild(icon);
        icon.getComponent(DockIconSc).haveDot = false; */
        this.login_completed = true;
    }
    onclick_start() {
        Msger.emit(Msger.on_sound_play, 1);
        let cancontinue = GameLocalStorage.gameData.inrevive == null && GameLocalStorage.gameData.gameSave;
        if (cancontinue) {
            Msger.emit(Msger.on_ui_show_message, Gamelanguage.getText(Gamelanguage.ask_continue_game), () => {
                Msger.emit(Msger.on_game_form_load);
                this.node.removeFromParent();
                GlobleValue.gameState = GameState.Runing;
                Msger.emit(Msger.on_sound_play, 4);
            }, this, () => {
                Msger.emit(Msger.on_level_progress_zero);//顶部的进度分数计算
                GameLocalStorage.gameData.gameSave = null;
                GameLocalStorage.gameData.currentScore = 0;
                GameLocalStorage.gameData.levelprogress = 0;
                GameLocalStorage.gameData.shotStep = 0;
                GameLocalStorage.gameData.levelprogressscore = 0;
                GameLocalStorage.gameData.wannengprop = 1;
                GameLocalStorage.gameData.wannenggetcount = 0;
                Msger.emit(Msger.on_game_reset_play);
                this.onclick_start();
            }, this);
        } else {
            Msger.emit(Msger.on_level_progress_zero);//顶部的进度分数计算
            Msger.emit(Msger.on_show_help_hand);
            GameLocalStorage.gameData.reviveTimes = 1;
            GameLocalStorage.gameData.wannengprop = 1;
            GameLocalStorage.gameData.wannenggetcount = 0;
            this.node.removeFromParent();
            GlobleValue.gameState = GameState.Runing;
            Msger.emit(Msger.on_sound_play, 4);
        }
        window.wxplatform.bannerAdShow();
    }
    onclick_free_gem() {
        //获取钻石和邀请一样
        Msger.emit(Msger.on_ui_how_get_gem);
    }
    on_ui_click_rank() {
        Msger.emit(Msger.on_ui_click_rank);
    }
    on_ui_click_skin() {
        Msger.emit(Msger.on_ui_click_skin);
    }
    onclick_sound() {
        Msger.emit(Msger.on_sound_mute_change);
    }
    onclick_share() {
        ComLogic.shareto("menu", false);
        Msger.emit(Msger.on_sound_play, 1);
    }
    //
    onclick_clear_save() {
        this.node.addChild(cc.instantiate(this.debugUI));
        Msger.emit(Msger.on_sound_play, 1);
    }
    ocnlcik_language(s, e) {
        GameLocalStorage.gameData.language = e;
        GameLocalStorage.Save();
        Msger.emit(Msger.on_change_Language);
    }
}
