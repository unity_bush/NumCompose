import { Msger } from "../Msger";
import { GlobleValue, GameState } from "../ComClass";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class HowGetGemUISc extends cc.Component {

    // LIFE-CYCLE CALLBACKS:

    onLoad () {

        window.wxplatform.createBannerAd();
    }

    start() {

    }

    // update (dt) {}
    onclick_close() {
        if(GlobleValue.gameState == GameState.Start){
            window.wxplatform.bannerAdHide();
        }
        this.node.destroy();
    }
    onclick_invite() {
        Msger.emit(Msger.on_ui_invite);
    }
    onclick_chengjiu() {
        Msger.emit(Msger.on_ui_click_achievement);
    }
}
