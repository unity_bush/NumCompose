import { Msger } from "../Msger";
import { ComLogic } from "../WxPlatfromLogic";

const { ccclass, property } = cc._decorator;

@ccclass
export default class HutuiIconSc extends cc.Component {
    onLoad() {
        Msger.on(Msger.on_network_login_completed, this.on_network_login_completed, this);
    }
    private on_network_login_completed() {
        this.chenageIcon();
    }
    private chenageCD: number = -1;
    update(dt) {
        if (this.chenageCD == -1) {
            return;
        }
        this.chenageCD += dt;
        if (this.chenageCD >= 10) {
            this.chenageIcon();
        }
    }
    private currentIndex = 0;
    private chenageIcon() {
        let [index, icon] = ComLogic.getNavigateToOtherIcon();
        if (index >= 0) {
            this.currentIndex = index;
            let sprite = this.getComponent(cc.Sprite);
            this.chenageCD = 0;
            cc.loader.load({
                url: icon, type: 'png'
            }, (err, texture) => {
                if (err) console.error(err);
                sprite.spriteFrame = new cc.SpriteFrame(texture);
            });
        }
    }
    onclick_self() {
        Msger.emit(Msger.on_sound_play, 1);
        this.chenageIcon();
        //出去
        ComLogic.navigateToOther(this.currentIndex);
    }
}
