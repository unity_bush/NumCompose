import { Msger } from "../Msger";
import AudioSystemSc from "../AudioSystemSc";

import { GlobleValue, GameState, Gamelanguage } from "../ComClass";
import { ComLogic } from "../WxPlatfromLogic";
import DockIconSc from "./DockIconSc";
const { ccclass, property } = cc._decorator;

@ccclass
export default class MenuUISc extends cc.Component {

    @property(cc.Node)
    blackbg: cc.Node = null;
    @property(cc.Node)
    items: cc.Node = null;
    @property(cc.Label)
    version: cc.Label = null;

    @property(cc.Node)
    soundOff: cc.Node = null;
    @property(cc.Node)
    soundOn: cc.Node = null;
    @property(cc.Prefab)
    dockIcon: cc.Prefab = null;
    @property(cc.Node)
    npcMouth1: cc.Node = null;
    @property(cc.Node)
    npcMouth2: cc.Node = null;
    @property(cc.Node)
    npcHand: cc.Node = null;

    @property(cc.Node)
    showPanel: cc.Node = null;
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.node.x = -640;
        Msger.on(Msger.on_network_login_completed, this.on_network_login_completed, this);
        Msger.on(Msger.on_ui_hide_menu, this.showUI, this);
        Msger.on(Msger.on_add_score, this.on_add_score, this);
    }
    showUI() {
        Msger.emit(Msger.on_sound_play, 1);
        this.node.stopAllActions();
        if (this.isClosed) {
            let targetX = this.items.width - 640;
            this.blackbg.x = -targetX;
            this.blackbg.active = true;
            GlobleValue.gameState = GameState.SubUI;
            this.node.runAction(cc.moveTo(0.3, cc.v2(targetX, 0)));
            this.isClosed = false;
        } else {
            this.blackbg.active = false;
            GlobleValue.gameState = GameState.Runing;
            this.node.runAction(cc.sequence(cc.moveTo(0.3, cc.v2(-640, 0)), cc.callFunc(this.closeAnimatOver, this)));

        }
    }
    private closeAnimatOver() {
        this.isClosed = true;
    }
    private isClosed = true;
    initUI() {
        let array = ComLogic.dockArray;
        if (!array) {
            return;
        }
        if (array.length <= 0) {
            return;
        }
        let docks: cc.Node[] = [];
        let row = this.items.children[0].childrenCount;//每个列多少行
        let count = array.length / row;
        count = Math.floor(count);
        if (count < 1) {
            count = 1;
        }
        for (let i = 0; i < count; i++) {
            let dock = cc.instantiate(this.items.children[0]);
            dock.removeAllChildren(true);
            docks.push(dock);
            this.items.addChild(dock);
        }
        let onedock = docks.shift();
        if (onedock) {
            for (let i = 0; i < array.length; i++) {
                let icon = cc.instantiate(this.dockIcon);
                icon.getComponent(DockIconSc).initData(array[i], 104, 112);
                onedock.addChild(icon);
                if (onedock.childrenCount >= row) {
                    onedock = docks.shift();
                    if (!onedock) {
                        break;
                    }
                }
            }
        }
    }

    on_add_score() {
        this.mouthTimer = 0.6;
    }
    start() {

    }
    private mouthTimer = 0;
    private handSpd = 10;
    update(dt) {
        let mute = AudioSystemSc.isMute;//静音判断
        this.soundOff.active = mute;
        this.soundOn.active = !mute;
        let rotation = this.npcHand.rotation + dt * this.handSpd;
        if (rotation > 20 || rotation < 0) {
            this.handSpd = -this.handSpd;
        } else {
            this.npcHand.rotation = rotation;
        }
        this.mouthTimer -= dt;
        if (this.mouthTimer < 0) {
            this.mouthTimer = 0;
        }
        this.npcMouth2.active = this.mouthTimer == 0;
        this.npcMouth1.active = this.mouthTimer > 0;
        this.showPanel.active = !this.isClosed;
    }
    hide_completed() {
        this.node.active = false;
        GlobleValue.gameState = GameState.Runing;
    }
    on_ui_click_home() {
        Msger.emit(Msger.on_ui_click_home);
        this.showUI();
        Msger.emit(Msger.on_subcontext_to_close);
    }
    on_ui_click_rank() {
        Msger.emit(Msger.on_ui_click_rank);
    }
    on_ui_click_achievement() {
        Msger.emit(Msger.on_ui_click_achievement);
    }
    on_ui_click_skin() {
        Msger.emit(Msger.on_ui_click_skin);
    }
    on_ui_click_dailytask() {
        Msger.emit(Msger.on_ui_click_dailytask);
    }
    on_click_more_game() {
        Msger.emit(Msger.on_menu_more_game);
        Msger.emit(Msger.on_sound_play, 1);
    }

    on_click_service() {
        window.wxplatform.openCustomerServiceConversation();
        Msger.emit(Msger.on_sound_play, 1);
    }

    on_click_share() {

    }
    on_click_sound() {
        Msger.emit(Msger.on_sound_mute_change);
    }

    private on_network_login_completed() {
        Msger.off(Msger.on_network_login_completed, this.on_network_login_completed, this);
        this.initUI();
    }
    on_click_replay() {        
        Msger.emit(Msger.on_ui_show_message, Gamelanguage.getText(Gamelanguage.ask_reset_game), () => {
            Msger.emit(Msger.on_game_reset_play);
            this.showUI();
        }, this, () => { }, this);
    }
}
