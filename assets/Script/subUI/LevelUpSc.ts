import { ComLogic } from "../WxPlatfromLogic";
import { GameLocalStorage } from "../ComClass";
import { Msger } from "../Msger";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class LevelUpSc extends cc.Component {

    @property(cc.Label)
    level: cc.Label = null;
    @property(cc.Sprite)
    spr: cc.Sprite = null;
    @property(cc.Sprite)
    spr2: cc.Sprite = null;
    @property(cc.Sprite)
    light: cc.Sprite = null;
    @property(cc.Node)
    afterShow: cc.Node = null;

    @property(cc.Node)
    beforShow: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:
    private index = 0;
    private frameTimer = 0;
    onLoad() {
        if (GameLocalStorage.gameData) {
            this.level.string = "Level:" + GameLocalStorage.gameData.level + "";
            this.afterShow.active = false;
            window.wxplatform.createBannerAd();
        }
    }
    animatComplated() {
        this.afterShow.active = true;
    }

    update(dt) {
        this.frameTimer += dt;
        if (this.frameTimer > 0.3) {
            this.frameTimer = 0;
            this.index += 1;
            if (this.index % 2 == 1) {
                //this.spr.spriteFrame = this.frame2;
                this.spr2.node.active = true;
            } else {
                //this.spr.spriteFrame = this.frame1;
                this.spr2.node.active = false;
            }
        }
        this.light.node.rotation += dt * 80;
    }
    onclick_close() {
        Msger.emit(Msger.on_sound_play, 1);
        //this.node.destroy();
        this.afterShow.active = false;
        this.beforShow.runAction(cc.sequence(cc.moveBy(0.5, cc.v2(0, 640)).easing(cc.easeBackIn()), cc.callFunc(this.node.destroy, this.node)));
        //关闭的时候送10个钻石
        Msger.emit(Msger.on_take_some_gem, 10, "levelup", false);
    }
    onclick_share() {
        ComLogic.shareto("levelup",false);
    }

}
