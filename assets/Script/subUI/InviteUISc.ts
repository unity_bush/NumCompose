import { GlobleValue, GameState } from "../ComClass";
import { ComLogic } from "../WxPlatfromLogic";
import { Msger } from "../Msger";
import InviteItemUISc from "./InviteItemUISc";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class InviteUISc extends cc.Component {

    @property(cc.Node)
    items: cc.Node = null;
    @property(cc.Prefab)
    item: cc.Prefab = null;
    @property(cc.Label)
    cooldown: cc.Label = null;
    // LIFE-CYCLE CALLBACKS:
    private max_invite = 10;
    onLoad() {
        Msger.on(Msger.on_invite_get, this.on_invite_get, this);
        this.initData();
    }
    private async initData() {

    }
    private inviteCountInfo: { invite: number, inviteNew: number, saveTime: number } = null;
    private inviteCountInfoKey = "PO2048InviteCountInfo";
    on_invite_get(isnew) {
        this.inviteCountInfo.inviteNew += 1;
        this.inviteCountInfo.invite += 1;
        localStorage.setItem(this.inviteCountInfoKey, JSON.stringify(this.inviteCountInfo));
    }
    start() {
        GlobleValue.gameState = GameState.SubUI;
    }

    // update (dt) {}
    onDestroy() {
        Msger.off(Msger.on_invite_get, this.on_invite_get, this);
    }
    onclick_close() {
        Msger.emit(Msger.on_sound_play,1);
        this.node.destroy();
    }
    onclick_invite() {
        Msger.emit(Msger.on_sound_play,1);
    }
}
