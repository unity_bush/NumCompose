import { GameData, GameLocalStorage, AchievementData } from "../ComClass";
import { ComLogic } from "../WxPlatfromLogic";
import { Msger } from "../Msger";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class SkinUiItemSc extends cc.Component {

    @property(cc.Node)
    markSelected: cc.Node = null;
    @property(cc.Label)
    skinName: cc.Label = null;

    @property(cc.Node)
    btnHelp: cc.Node = null;
    @property(cc.Node)
    btnGot: cc.Node = null;
    @property(cc.Node)
    btnbuy: cc.Node = null;
    @property(cc.Node)
    btnChengjiu: cc.Node = null;
    @property(cc.Sprite)
    icon: cc.Sprite = null;
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
    }
    private themekey = 0;
    private cost: number = 0;
    setSkinData(key, data: { name, gettype, cost, icon }) {
        this.themekey = key;
        this.cost = data.cost;
        this.btnbuy.active = this.btnChengjiu.active = this.btnGot.active = this.btnChengjiu.active = false;
        this.skinName.string = data.name;
        if (GameLocalStorage.gameData.gotThemes.indexOf(key) >= 0) {
            this.btnGot.active = true;
        } else {
            switch (data.gettype) {
                case 0:
                    this.btnGot.active = true;
                    break;
                case 1:
                    this.btnHelp.active = true;
                    break;
                case 2:
                    this.btnbuy.active = true;
                    this.btnbuy.children[0].getComponent(cc.Label).string = data.cost;
                    break;
                case 3:
                    this.btnChengjiu.active = true;
                    break;
            }
        }
        this.icon.spriteFrame = data.icon;
    }
    isSelected: boolean = false;
    update(dt) {
        this.markSelected.active = this.isSelected;
    }
    onclick_chengjiu() {
        Msger.emit(Msger.on_sound_play,1);
        Msger.emit(Msger.on_ui_click_achievement);
    }
    onclick_buy() {
        Msger.emit(Msger.on_sound_play,1);
        if (GameLocalStorage.gameData.gem < this.cost) {
            Msger.emit(Msger.on_ui_show_message, "宝石不足");
        }else{
            GameLocalStorage.gameData.gotThemes.push(this.themekey);            
            GameLocalStorage.gameData.gem -= this.cost;
            Msger.emit(Msger.on_ui_show_message, "解锁成功\n快去试试看吧！");
            this.btnbuy.active = false;
            this.btnGot.active = true;
            GameLocalStorage.Save();
        }
    }
    async onclick_help() {
        Msger.emit(Msger.on_sound_play,1);
        let ret = await ComLogic.shareto("skin", true);
        if (ret) {
            GameLocalStorage.gameData.gotThemes.push(this.themekey);
            this.btnHelp.active = false;
            this.btnGot.active = true;
            Msger.emit(Msger.on_ui_show_message, "解锁成功\n快去试试看吧！");
            GameLocalStorage.Save();
        }
    }
    onclick_self() {
        Msger.emit(Msger.on_sound_play,1);
        if (GameLocalStorage.gameData.gotThemes.indexOf(this.themekey) >= 0) {
            for (let item of this.node.parent.children) {
                if (item == this.node) {
                    continue;
                }
                item.getComponent(SkinUiItemSc).isSelected = false;
            }
            this.isSelected = true;
            GameLocalStorage.gameData.themekey = this.themekey;
            Msger.emit(Msger.on_change_theme);
        } else {
            Msger.emit(Msger.on_ui_show_message, "您还未拥有这个皮肤");
        }
    }
}
