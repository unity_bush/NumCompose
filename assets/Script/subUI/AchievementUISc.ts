import { GlobleValue, GameState, AchievementData } from "../ComClass";
import AchievementItemUISc from "./AchievementItemUISc";
import { Msger } from "../Msger";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class AchievementUISc extends cc.Component {

    @property(cc.Node)
    items: cc.Node = null;
    @property(cc.Prefab)
    item: cc.Prefab = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        let array1: cc.Node[] = [];
        let array2: cc.Node[] = [];
        for (let i = 0; i < AchievementData.AchievementConfig.length; i++) {
            let data = AchievementData.AchievementConfig[i];
            let item = cc.instantiate(this.item);
            item.getComponent(AchievementItemUISc).updateData(data);
            if (AchievementData.haveAchievement.indexOf(data.aID) >= 0) {
                array2.push(item);
            } else {
                array1.push(item);
            }
        }
        array1.sort((a,b)=>{
            return b.getComponent(AchievementItemUISc).progressBar.progress - a.getComponent(AchievementItemUISc).progressBar.progress;
        });
        for (let i = 0; i < array1.length; i++) {
            this.items.addChild(array1[i]);
        }
        for (let i = 0; i < array2.length; i++) {
            this.items.addChild(array2[i]);
        }
    }

    start() {
        GlobleValue.gameState = GameState.SubUI;
    }

    // update (dt) {}

    onclick_close() {
        Msger.emit(Msger.on_sound_play,1);
        this.node.destroy();
    }
}
