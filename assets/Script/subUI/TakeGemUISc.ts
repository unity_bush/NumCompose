import { ComLogic } from "../WxPlatfromLogic";
import { GameLocalStorage, GlobleValue } from "../ComClass";
import { Msger } from "../Msger";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class TakeGemUISc extends cc.Component {

    @property(cc.Label)
    gem: cc.Label = null;
    @property(cc.Label)
    title: cc.Label = null;
    @property(cc.Label)
    btnDoubleLabel: cc.Label = null;
    @property(cc.Node)
    light: cc.Node = null;
    @property(cc.Node)
    btnDouble: cc.Node = null;
    @property(cc.Node)
    doubleGemShow: cc.Node = null;
    @property(cc.Node)
    btnGet: cc.Node = null;
    gemCount: number = 0;
    candouble: boolean = false;
    public setGemCount(count, fromkey, candouble, title = "") {
        this.candouble = candouble;
        this.gemCount = count;
        this.gem.string = "x" + this.gemCount;
        this.fromKey = fromkey;
        this.title.string = title;
        if (candouble) {
            if (GlobleValue.gameSwitch) {
                //this.btnDoubleLabel.string = "看视频双倍";
            }
        }
    }
    public fromKey = "";
    // LIFE-CYCLE CALLBACKS:
    onclick_get() {
        GameLocalStorage.gameData.gem += this.gemCount;
        GameLocalStorage.Save();
        this.btnGet.active = false;
        this.node.destroy();
        Msger.emit(Msger.on_sound_play, 1);
    }
    async onclick_double() {
        /* let ret = await ComLogic.shareGoto(this.fromKey, true);
        if (ret) {
            this.btnDouble.active = false;
            this.doubleGemShow.active = true;
            this.btnGet.active = false;
            this.doubleGemShow.runAction(cc.sequence(cc.delayTime(0.5), cc.fadeTo(1, 0)));
            this.doubleGemShow.runAction(cc.sequence(cc.delayTime(0.5), cc.moveBy(1, cc.v2(0, 100)).easing(cc.easeSineOut())));
            this.node.runAction(cc.sequence(cc.delayTime(1.5), cc.callFunc(() => {
                this.setGemCount(this.gemCount * 2, this.fromKey, false, this.title.string);
                //this.btnGet.active = true;
                this.onclick_get();
            }, this)))
        } */
        Msger.emit(Msger.on_sound_play, 1);
    }

    onLoad() {
        this.btnDouble.active = false;
        this.btnGet.active = false;
        this.doubleGemShow.active = false;
    }
    playsound() {
        //播放声音才对
    }
    amiComplated() {
/*         if (this.candouble) {
            //this.btnDouble.active = GlobleValue.gameSwitch == false;
            this.btnDouble.active = true;
        } else {
            this.btnDouble.active = false;
        } */
        this.btnDouble.active = false;
        this.btnGet.active = true;
    }
    update(dt) {
        if (this.light) {
            this.light.rotation += dt * 80
        }
        /* if (this.btnDouble) {
            this.btnDouble.scale += dt * 0.2;
            if (this.btnDouble.scale >= 1.1) {
                this.btnDouble.scale = 0.9;
            }
        } */
    }
}
