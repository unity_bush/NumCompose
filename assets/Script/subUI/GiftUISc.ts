import { Msger } from "../Msger";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class GiftUISc extends cc.Component {

    @property(cc.Node)
    light: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        window.wxplatform.createBannerAd();
    }

    update (dt) {
        this.light.rotation += dt * 100;
    }
    onclick_skip(){
        Msger.emit(Msger.on_sound_play,1);
        this.node.destroy();
    }
    onclick_get(){
        Msger.emit(Msger.on_sound_play,1);
        this.node.destroy();
    }
}
