

export module Msger {
    export let on_game_add_effect = "on_game_add_effect";
    export let on_game_reset_play = "on_game_reset_play";
    export let on_game_die = "on_game_die";
    export let on_game_over = "on_game_over";
    export let on_game_revive = "on_game_revive";
    export let on_game_remove_ball = "on_game_remove_ball";
    export let on_game_state_change = "on_game_state_change";
    export let on_add_score = "on_add_score";
    export let on_reset_score = "on_reset_score";
    export let on_game_form_load = "on_game_form_load";
    export let on_game_to_save = "on_game_to_save";
    // msessage
    export let on_ui_show_message = "on_ui_show_message";
    // 点击菜单
    export let on_ui_click_menu = "on_ui_click_menu";
    // 点击返回到开始UI
    export let on_ui_click_home = "on_ui_click_home";
    // 点击 菜单里的更多游戏
    export let on_menu_more_game = "on_menu_more_game";
    // 点击排行
    export let on_ui_click_rank = "on_ui_click_rank";
    // 点击成就
    export let on_ui_click_achievement = "on_ui_click_achievement";
    // 点击皮肤
    export let on_ui_click_skin = "on_ui_click_skin";
    // 获得钻石
    export let on_take_some_gem = "on_take_some_gem";
    // 点击 每日任务
    export let on_ui_click_dailytask = "on_ui_click_dailytask";
    // 点击 使用道具
    export let on_ui_click_prop = "on_ui_click_prop";
    // 点击 使用道具
    export let on_ui_invite = "on_ui_invite";
    // 出现道具礼物UI
    export let on_ui_prop_gift = "on_ui_prop_gift";
    // 点击 使用道具
    export let on_ui_how_get_gem = "on_ui_how_get_gem";

    // 改变到排行
    export let on_subcontext_to_rank = "on_subcontext_to_rank";
    // 改变到游戏结束
    export let on_subcontext_to_over = "on_subcontext_to_over";
    // 改变到游戏内超越
    export let on_subcontext_to_exceed = "on_subcontext_to_exceed";
    // 关闭
    export let on_subcontext_to_close = "on_subcontext_to_close";

    // 关卡进度重置
    export let on_level_progress_zero = "on_level_progress_zero";
    // 关卡进度同步
    export let on_level_progress_update = "on_level_progress_update";
    // 关卡进度重置
    export let on_level_up = "on_level_up";

    // 声音静音调整
    export let on_ui_hide_menu = "on_ui_hide_menu";
    // 声音静音调整
    export let on_sound_mute_change = "on_sound_mute_change";
    // 声音球的
    export let on_sound_play_ball = "on_sound_play_ball";
    // 声音普通
    export let on_sound_play = "on_sound_play";
    // 改变皮肤
    export let on_change_theme = "on_change_theme";
    // 领取到了邀请奖励,参数新的或者老的
    export let on_invite_get = "on_invite_get";
    // 显示帮助手
    export let on_show_help_hand = "on_show_help_hand";
    // 显示分享阻挡
    export let on_show_block_shareui = "on_show_block_shareui";
    // 隐蔽分享阻挡
    export let on_hide_block_shareui = "on_hide_block_shareui";

    export let on_network_login_completed = "on_network_login_completed";
    // 改变语言
    export let on_change_Language = "on_change_theme";

    let _msgerNode: cc.Node = null;
    function msgerNode(): cc.Node {
        if (_msgerNode == null) {
            _msgerNode = new cc.Node();
        }
        return _msgerNode;
    }
    export function on(type: string, callback: (event: cc.Event.EventCustom) => void, target?: any) {
        msgerNode().on(type, callback, target);
    }
    export function off(type: string, callback?: Function, target?: any): void {
        msgerNode().off(type, callback, target);
    }
    export function once(type: string, callback: (arg1?: any, arg2?: any, arg3?: any, arg4?: any, arg5?: any) => void, target?: any): void {
        msgerNode().once(type, callback, target);
    }
    export function emit(type: string, arg1?: any, arg2?: any, arg3?: any, arg4?: any, arg5?: any): void {
        msgerNode().emit(type, arg1, arg2, arg3, arg4, arg5);
    }
}