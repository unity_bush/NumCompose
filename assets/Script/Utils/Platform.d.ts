
declare interface Platform {
    inWxgame();
    login(): Promise<any>;
    getUserInfo(): Promise<any>;
    shareAppMessage(title, imgurl, data): Promise<any>;
    getLaunchOptionsSync();
    showShareMenu();
    onShareAppMessage(callback);
    getSettingUserInfo(): Promise<any>;
    getSystemInfoSync(): any;
    destroyInfoButton();
    createInfoButton(x, y, w, h): Promise<any>;
    playSound(url);
    postMessage(data);
    getOpenDataContext();
    onShow(callback);
    offShow(callback);
    showLoading(title);
    hideLoading();
    createBannerAd();
    bannerAdShow();
    bannerAdHide();
    createRewardedVideoAd(): Promise<any>;
    createImage(): any;
    showMessageBoxOK(title, content, confirmText);
    openCustomerServiceConversation();
    showMessageBoxOKCancel(title, content, confirmText): Promise<any>;
    getSetting(): Promise<any>;
    navigateToMiniProgram(appId, path, pic, needQR);
    vibrateShort();
    getShareInfo(shareTickets): Promise<any>;
}


declare interface Window {
    wxplatform: Platform
}