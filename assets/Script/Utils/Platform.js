
class wxPlatform {
    inWxgame() { return true; }
    login() {
        return new Promise((reslove, reject) => {
            wx.login({
                success: (res) => {
                    reslove(res)
                }
            })
        })
    }
    getUserInfo() {
        return new Promise((resolve, reject) => {
            wx.getUserInfo({
                withCredentials: true,
                success: function (res) {
                    let userInfo = res.userInfo
                    resolve(res);
                },
                fail(err) {
                    resolve(null);
                }
            })
        })
    }
    shareAppMessage(title, imgurl, data) {
        return new Promise((resolve, reject) => {
            wx.shareAppMessage({
                title: title,
                imageUrl: imgurl,
                query: data,
                success: (res) => {
                    console.log("shareAppMessage success", res);
                    resolve(res);
                },
                fail: (err) => {
                    resolve(null);
                }
            });
        })
    }

    getSettingUserInfo() {
        return new Promise((resolve, reject) => {
            wx.getSetting({
                success: (res) => {
                    resolve(res.authSetting['scope.userInfo']);
                    console.log("getSetting", res)
                }
            })
        })
    }
    getLaunchOptionsSync() {
        return wx.getLaunchOptionsSync();
    }

    showShareMenu() {
        wx.showShareMenu({
            withShareTicket: true
        });
    }
    createImage() {
        return wx.createImage();
    }
    onShareAppMessage(callback) {
        wx.onShareAppMessage(callback);
    }
    getShareInfo(shareTickets) {
        return new Promise((resolve, reject) => {
            if (!shareTickets) {
                resolve(null);
            }
            wx.getShareInfo({
                shareTicket: shareTickets,
                success: function (res) {
                    resolve(res);
                    //res.errMsg,res.encryptedData,res.iv
                },
                fail: function (res) {
                    resolve(null);
                }
            })
        });
    }
    showLoading(title) {
        wx.showLoading({
            title: title, mask: true
        });
    }
    hideLoading() {
        wx.hideLoading();
    }
    showAdv(advurl) {
        wx.previewImage({
            urls: advurl,
        });
    }
    getSystemInfoSync() {
        return wx.getSystemInfoSync();
    }
    infoButton = null;
    destroyInfoButton() {
        if (this.infoButton) {
            this.infoButton.destroy();
            this.infoButton = null;
        }
    }
    createInfoButton(x, y, w, h) {
        if (this.infoButton) {
            this.destroyInfoButton();
        }
        let button = this.infoButton = wx.createUserInfoButton({
            type: 'text',
            text: '',
            style: {
                left: 0,
                top: 0,
                width: 640,
                height: 1136,
                lineHeight: 40,
                backgroundColor: "transparent",
                color: '#ffffff',
                textAlign: 'center',
                fontSize: 16,
                borderRadius: 4
            }
        })
        return new Promise((resolve, reject) => {
            button.onTap((res) => {
                console.log(res);
                resolve(res);
            })
        })
    }
    playSound(url) {
        let sound = wx.createInnerAudioContext();
        sound.src = url;
        sound.loop = false;
        sound.play();
    }
    openDataContext = null;
    getOpenDataContext() {
        return this.openDataContext = wx.getOpenDataContext();
    }
    postMessage(data) {
        if (this.openDataContext == null) {
            this.getOpenDataContext();
        }
        this.openDataContext.postMessage(data);
    }
    onShow(callback) {
        wx.onShow(callback);
    }
    offShow(callback) {
        wx.offShow(callback);
    }
    bannerAd = null;
    createBannerAd() {
        return;
        let info = wx.getSystemInfoSync();
        if (!this.compareVersion(info.SDKVersion, "2.0.5")) {
            return;
        }
        if (this.bannerAd != null) {
            this.bannerAd.destroy();
        }

        let w = 300;
        let x = (info.screenWidth - w) / 2;
        let y = info.screenHeight;
        this.bannerAd = wx.createBannerAd({
            adUnitId: 'adunit-1feb477f561b313a',
            style: {
                left: x,
                top: 0,
                width: w
            }
        })
        this.bannerAd.onResize(res => {
            console.log(res.width, res.height)
            //console.log(bannerAd.style.realWidth, bannerAd.style.realHeight)
            this.bannerAd.style.top = y - res.height;
            if (info.screenHeight < 667) {
                this.bannerAd.style.top += 12;
            }
            console.log(this.bannerAd.style.top);

        })
        this.bannerAd.onError((res) => {
            console.log('bannerAd.onError:', res.errMsg, res.errCode)
        });
        this.bannerAd.onLoad(() => {
            //console.log('banner 广告加载成功')
        })
        this.bannerAd.show();//.then(() => console.log('banner 广告显示'));
    }
    bannerAdShow() {
        if (this.bannerAd) {
            this.bannerAd.show();
        }
    }

    bannerAdHide() {
        if (this.bannerAd) {
            this.bannerAd.hide();
        }
    }
    createRewardedVideoAd() {
        return new Promise((resolve, reject) => {
            let rewardedVideoAd = wx.createRewardedVideoAd({ adUnitId: 'adunit-ef3d9765f986eb56' })
            rewardedVideoAd.onError((res) => { 
                console.log('rewardedVideoAd.onError:', res.errMsg, res.errCode)
                resolve(null) 
            });
            rewardedVideoAd.load()
                .then(() => { resolve(rewardedVideoAd); })
                .catch(err => resolve(null));
        })
    }
    compareVersion(curV, reqV) {
        if (curV && reqV) {
            //将两个版本号拆成数字
            var arr1 = curV.split('.'),
                arr2 = reqV.split('.');
            var minLength = Math.min(arr1.length, arr2.length),
                position = 0,
                diff = 0;
            //依次比较版本号每一位大小，当对比得出结果后跳出循环（后文有简单介绍）
            while (position < minLength && ((diff = parseInt(arr1[position]) - parseInt(arr2[position])) == 0)) {
                position++;
            }
            diff = (diff != 0) ? diff : (arr1.length - arr2.length);
            //若curV大于reqV，则返回true
            return diff > 0;
        } else {
            //输入为空
            console.log("版本号不能为空");
            return false;
        }
    }
    //提示
    showMessageBoxOK(title, content, confirmText) {
        wx.showModal({
            title: title, content: content, confirmText: confirmText, showCancel: false
        })
    }
    openCustomerServiceConversation() {
        wx.openCustomerServiceConversation();
    }

    navigateToMiniProgram(appId, path, pic, needQR) {
        console.log("navigateToMiniProgram:", appId, path, pic, needQR);
        let info = wx.getSystemInfoSync();
        if (this.compareVersion(info.SDKVersion, "2.1.9") && !needQR) {
            wx.navigateToMiniProgram({
                appId: appId,
                path: path,
                fail: res => {
                    console.log(res)
                },
                success: res => {
                    console.log(res)
                }
            });
        } else {
            this.showAdv([pic]);
        }
    }

    getSetting() {
        return new Promise((resolve, reject) => {
            wx.getSetting({
                success: (res) => {
                    resolve(res);
                },
                fail: (f) => {
                    resolve(null);
                }
            });
        });
    }

    showMessageBoxOKCancel(title, content, confirmText) {
        return new Promise((resolve, reject) => {
            wx.showModal({
                title: title, content: content, confirmText: confirmText, showCancel: true,
                cancelText: "取消",
                success: function (res) {
                    let confirm = res.confirm;
                    resolve(!!confirm);
                },
                fail: function (res) {
                    resolve(false);
                }
            })
        });
    }
    vibrateShort() {
        wx.vibrateShort({});
    }
}
if (!window.wxplatform && cc.sys.WECHAT_GAME == cc.sys.platform) {
    window.wxplatform = new wxPlatform();
}
