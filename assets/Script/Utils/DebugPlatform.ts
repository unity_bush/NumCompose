
class DebugPlatform {
    inWxgame() { return false; }
    login(): Promise<any> { return null; }
    getUserInfo(): Promise<any> { return null; }
    shareAppMessage(title, imgurl, data): Promise<any> { return null; }
    getLaunchOptionsSync() { return {} }
    showShareMenu() { }
    onShareAppMessage(callback) { }
    getSettingUserInfo(): Promise<any> { return null; }
    getSystemInfoSync(): any { return null; }
    destroyInfoButton() { }
    createInfoButton(x, y, w, h): Promise<any> { return null; }
    playSound(url) { }
    postMessage(data) { }
    getOpenDataContext() { }
    onShow(callback) { }
    offShow(callback) { }
    showLoading(title) { }
    hideLoading() { }
    createBannerAd() { }
    bannerAdShow() { }
    bannerAdHide() { }
    createRewardedVideoAd(): Promise<any> { return null; }
    createImage(): any { return null; }
    showMessageBoxOK(title, content, confirmText) { return null; }
    openCustomerServiceConversation() { }
    showMessageBoxOKCancel(title, content, confirmText): Promise<any> { return null; }
    getSetting(): Promise<any> { return null; }
    navigateToMiniProgram(appId, path, pic, needQR) { }
    vibrateShort() { }
    getShareInfo(shareTickets): Promise<any> { return null; }
}
if (!window.wxplatform && cc.sys.WECHAT_GAME != cc.sys.platform) {
    window.wxplatform = new DebugPlatform();
}
