// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class LogoSceneSc extends cc.Component {

    @property(cc.Node)
    logo: cc.Node = null;


    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        let self = this;
        cc.director.preloadScene("Scene/Main", (completedcount, totalcount) => {

        }, (error) => {
            self.timer = 1;
        });
    }
    timer: number = 3;
    update(dt) {
        if (this.timer > 0) {
            this.timer -= dt;
            this.logo.opacity = 255 * (this.timer);
            if (this.timer <= 0) {
                this.netxScene();
            }
        }
    }    
    netxScene() {
        cc.director.loadScene("Scene/Main");
    }
}
