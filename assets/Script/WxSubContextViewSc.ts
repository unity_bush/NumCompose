import { Msger } from "./Msger";
import { GameLocalStorage, GlobleValue, GameState } from "./ComClass";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class WxSubContextViewSc extends cc.Component {
    /*     //排行视图
        @property(cc.Node)
        rankView: cc.Node = null;
        //结束视图
        @property(cc.Node)
        overView: cc.Node = null;
        //超越视图
        @property(cc.Node)
        exceedView: cc.Node = null; */

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        //this.getComponent(cc.WXSubContextView).enabled = false;
        Msger.on(Msger.on_subcontext_to_close, this.on_subcontext_to_close, this);
        Msger.on(Msger.on_subcontext_to_rank, this.on_subcontext_to_rank, this);
        Msger.on(Msger.on_subcontext_to_over, this.on_subcontext_to_over, this);
        Msger.on(Msger.on_subcontext_to_exceed, this.on_subcontext_to_exceed, this);
        this.node.active = false;
    }

    start() {

    }

    update(dt) {

    }
    private on_subcontext_to_close() {
        this.node.active = false;
        window.wxplatform.postMessage({ view: "close" });
    }

    private on_subcontext_to_rank() {
        window.wxplatform.postMessage({ view: "close" });
        this.node.x = this.node.y = 0;
        this.node.width = 640;
        this.node.height = 1136;
        this.node.active = true;
        window.wxplatform.postMessage({
            view: "rank",
        });
        //let sc = this.getComponent(cc.WXSubContextView);
    }

    private on_subcontext_to_over() {
        window.wxplatform.postMessage({ view: "close" });
        this.node.x = this.node.y = 0;
        this.node.width = 640;
        this.node.height = 1136;
        this.node.active = true;
        window.wxplatform.postMessage({
            view: "over",
            score: GameLocalStorage.gameData.currentScore
        });
        //this.getComponent(cc.WXSubContextView).updateSubContextViewport();
    }

    private on_subcontext_to_exceed() {
        if (this.node.active == false) {
            this.node.active = true;
        }
        this.node.x = 260
        this.node.y = 448;
        this.node.width = 80;
        this.node.height = 135;
        window.wxplatform.postMessage({
            view: "exceed",
            score: GameLocalStorage.gameData.currentScore
        });
        //this.getComponent(cc.WXSubContextView).update();
        //this.getComponent(cc.WXSubContextView).updateSubContextViewport();
    }
}
