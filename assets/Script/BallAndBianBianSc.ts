import { Msger } from "./Msger";
import { GameLocalStorage } from "./ComClass";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class BallAndBianBianSc extends cc.Component {

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {

    }
    public DestroyBianbian() {
        for (let bianbian of this.bianbian) {
            bianbian.destroy();
            Msger.emit(Msger.on_game_add_effect, { key: "bao", radius: 131 / 2, back: true, color: cc.color(42, 19, 2, 1), x: bianbian.x, y: bianbian.y });            
            Msger.emit(Msger.on_sound_play, 9);
            GameLocalStorage.gameData.bianbian += 1;
        }
    }
    private bianbian: cc.Node[] = [];
    // update (dt) {}
    onBeginContact(contact, selfCollider, otherCollider) {
        if (otherCollider.node.name == "bianbian") {
            if (this.bianbian.indexOf(otherCollider.node) < 0) {
                this.bianbian.push(otherCollider.node);
            }
        }
    }

    onPreSolve(contact, selfCollider, otherCollider) {
        if (otherCollider.node.name == "bianbian") {
            if (this.bianbian.indexOf(otherCollider.node) < 0) {
                this.bianbian.push(otherCollider.node);
            }
        }
    }
    onEndContact(contact, selfCollider, otherCollider) {
        if (otherCollider.node.name == "bianbian") {
            let index = this.bianbian.indexOf(otherCollider.node);
            if (index >= 0) {
                this.bianbian.splice(index, 1);
            }
        }
    }
}
