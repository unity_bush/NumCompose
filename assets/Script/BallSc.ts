import { Msger } from "./Msger";
import { GlobleValue, GameState, GameLocalStorage, GameData } from "./ComClass";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class BallSc extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;
    @property(cc.Sprite)
    text: cc.Sprite = null;
    @property
    public size = 0;
    public static setHaveGiftIcon(ballsc: BallSc) {
        if (ballsc.haveGiftIcon) {
            return;
        }
        let icon = ballsc.haveGiftIcon = new cc.Node();
        icon.addComponent(cc.Sprite);
        icon.getComponent(cc.Sprite).spriteFrame = GameData.ball_gift_icon;
        ballsc.node.addChild(icon);
    }
    haveGiftIcon: cc.Node;
    // LIFE-CYCLE CALLBACKS:
    ballType = 0;//0 标准泡泡 1 万能泡泡
    setBallType(type) {
        if (type == this.ballType) {
            return;
        }
        this.ballType = type;
        this.label.string = "万能";
        this.getComponent(cc.Sprite).spriteFrame = GameData.ball_wanneng;
        let PhysicsCircleCollider = this.getComponent(cc.PhysicsCircleCollider);
        PhysicsCircleCollider.radius = 111 / 2;
        PhysicsCircleCollider.apply();
        this.text.destroy();
        this.node.runAction(cc.repeatForever(
            cc.sequence(cc.scaleTo(0.3, 0.7, 0.7), cc.scaleTo(0.3, 0.8, 0.8))
        ))
    }
    onLoad() {
        Msger.on(Msger.on_change_theme, this.on_change_theme, this);
        this.init();
    }
    shoting = true;
    public static ballsRadius: number[] = [50, 60, 76, 103, 115, 130, 145, 145, 159, 168, 178, 178, 178];
    public static ballsColors: cc.Color[] = [cc.color(196, 0, 0, 1), cc.color(235, 120, 0, 1), cc.color(255, 177, 0, 1), cc.color(53, 222, 0, 1), cc.color(57, 83, 244, 1), cc.color(56, 0, 149, 1), cc.color(247, 0, 242, 1), cc.color(219, 0, 29, 1), cc.color(255, 115, 0, 1), cc.color(255, 199, 0, 1), cc.color(53, 222, 0, 1), cc.color(65, 91, 252, 1), cc.color(65, 0, 65, 1)];
    private static sizeArray: number[] = [1, 1, 1, 2, 2, 2, 3, 3, 3, 4];
    private static sizeIndex: number = 0;
    private static randomArray() {
        if (BallSc.sizeIndex != 0) {
            return;
        }
        let array = [];
        for (let i = 0; i < 10; i++) {
            let index = Math.floor(Math.random() * BallSc.sizeArray.length);
            array.push(BallSc.sizeArray[index]);
            BallSc.sizeArray.splice(index, 1);
        }
        BallSc.sizeArray = array;
    }
    start() {
    }
    public init() {
        this.node.scale = 1;
        this.shoting = true;
        BallSc.randomArray();
        if (this.size == 0) {
            this.size = BallSc.sizeArray[BallSc.sizeIndex];
        }
        let rate = Math.pow(2, this.size);
        BallSc.sizeIndex += 1;
        if (BallSc.sizeIndex >= BallSc.sizeArray.length) {
            BallSc.sizeIndex = 0;
        }
        this.label.string = rate.toString();
        this.on_change_theme();
        this.label.node.active = false;
    }
    onBeginContact(contact, selfCollider, otherCollider) {
        if (GlobleValue.gameState != GameState.Runing) {
            return;
        }

        if (this.shoting == false && otherCollider.node.group == "deadline") {
            console.log("Game Over");
            GlobleValue.gameState = GameState.Over;
            Msger.emit(Msger.on_game_die);
            Msger.emit(Msger.on_sound_play, 3);
            return;
        } else if (this.shoting && otherCollider.node.group == "deadline") {
            contact.disabled = true;
            return;
        }
        if (this.ballType != 0) {
            return;
        }
        let otherballsc: BallSc = otherCollider.getComponent(BallSc);
        if (otherballsc) {
            if ((this.size == otherballsc.size || otherballsc.ballType == 1) && this.size < BallSc.ballsRadius.length) {
                //if (this.holdTimer <= 0 && otherballsc.holdTimer <=0) 
                {
                    contact.disabled = true;
                    if (otherCollider.node.parent) {
                        Msger.emit(Msger.on_game_add_effect, { key: "bao", radius: BallSc.ballsRadius[this.size - 1], back: true, color: BallSc.ballsColors[this.size - 1], x: otherCollider.node.x, y: otherCollider.node.y })
                        this.addSize(otherCollider.node);
                        if (otherballsc.ballType == 1) {//特殊球
                            if (otherballsc.haveGiftIcon) {
                                Msger.emit(Msger.on_ui_prop_gift);
                            }
                        }
                        Msger.emit(Msger.on_game_remove_ball, otherCollider.node, this.node);
                        Msger.emit(Msger.on_add_score, 20);
                    }
                }
            } else {
                if (this.shoting && otherCollider.node.group == "ball") {
                    this.shoting = false;
                    let sc = 1;
                    this.node.runAction(
                        cc.sequence(
                            cc.scaleTo(0.1, sc * 0.9, sc).easing(cc.easeBackInOut()),
                            cc.scaleTo(0.1, sc * 1, sc).easing(cc.easeBackInOut()),
                            cc.scaleTo(0.1, sc * 0.9, sc).easing(cc.easeBackInOut()),
                            cc.scaleTo(0.1, sc * 1, sc).easing(cc.easeBackInOut())
                        )
                    )
                    this.node.runAction(
                        cc.sequence(
                            cc.scaleTo(0.1, sc, sc * 1.1).easing(cc.easeBackInOut()),
                            cc.scaleTo(0.1, sc, sc * 1).easing(cc.easeBackInOut()),
                            cc.scaleTo(0.1, sc, sc * 1.1).easing(cc.easeBackInOut()),
                            cc.scaleTo(0.1, sc, sc * 1).easing(cc.easeBackInOut())
                        )
                    )
                }
            }
        }
    }
    //增加大小
    public addSize(otherBall: cc.Node) {
        Msger.emit(Msger.on_sound_play_ball, this.size);
        window.wxplatform.vibrateShort();
        Msger.emit(Msger.on_game_add_effect, { key: "bao", radius: BallSc.ballsRadius[this.size - 1], back: true, color: BallSc.ballsColors[this.size - 1], x: otherBall.x, y: otherBall.y })

        this.size += 1;
        this.updateSize();
        this.holdTimer = 0.2;
        //this.node.runAction(cc.scaleTo(0.2, BallSc.scales[this.size], BallSc.scales[this.size]).easing(cc.easeBackInOut()))
    }
    public updateSize() {
        let rate = Math.pow(2, this.size);
        this.label.string = rate.toString();
        if (rate > GameLocalStorage.gameData.max) {
            GameLocalStorage.gameData.max = rate;
            Msger.emit(Msger.on_game_to_save);
        }
        this.on_change_theme();
    }
    onDestroy() {
        if (this.haveGiftIcon) {
            this.haveGiftIcon.removeFromParent(true);
            this.haveGiftIcon = null;
        }
        Msger.off(Msger.on_change_theme, this.on_change_theme, this);
    }
    private on_change_theme() {
        if (this.ballType == 1) {
            return;
        }
        let theme = GameData.getBallTheme(this.size - 1);
        if (theme) {
            this.getComponent(cc.Sprite).spriteFrame = theme.bframe;
            let PhysicsCircleCollider = this.getComponent(cc.PhysicsCircleCollider);
            PhysicsCircleCollider.radius = BallSc.ballsRadius[this.size - 1] / 2;
            PhysicsCircleCollider.apply();
            if (!this.text) {
                this.text = this.node.getChildByName("number").getComponent(cc.Sprite);
            }
            this.text.spriteFrame = theme.tframe;
        }
    }
    //碰撞等待时间，避免瞬间消除
    private holdTimer = 0;
    update(dt) {
        this.holdTimer -= dt;
    }
}
