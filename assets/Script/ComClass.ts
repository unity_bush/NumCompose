export module GameData {
    let ballsThemes = {};
    export let themeConfig = {
        0: {
            name: "五彩泡泡", ball: "b1", text: "s1", gettype: 0, cost: 0, icon: null
        },
        /* 1: {
            name: "水果泡泡", ball: "b3", text: "", gettype: 1, cost: 0, icon: null
        },
        2: {
            name: "球类泡泡", ball: "b4", text: "", gettype: 2, cost: 2000, icon: null
        },
        3: {
            name: "星球泡泡", ball: "b2", text: "s2", gettype: 2, cost: 4000, icon: null
        } */
    };
    export let wannengSteps: number[] = [10, 30, 60, 100, 150, 210, 300];
    export let ball_wanneng: cc.SpriteFrame = null;
    export let ball_gift_icon: cc.SpriteFrame = null;
    export async function loadBallsThemes() {
        let balls: cc.SpriteFrame[];
        let texts: cc.SpriteFrame[];
        let ballbuff = [];
        let textbuff = [];
        //let icons = await loadTextrueDirSync("ball/icons");
        for (let key in themeConfig) {
            themeConfig[key].icon = await loadTextrueSync("ball/icons/" + key);
            let ballkey = themeConfig[key].ball;
            if (ballbuff[ballkey]) {
                balls = ballbuff[ballkey];
            } else {
                balls = await loadTextrueDirSync("ball/" + ballkey);
                balls = balls.sort((a, b) => {
                    return parseInt(a.name) - parseInt(b.name);
                });
                ballbuff[ballkey] = balls;
            }
            let textkey = themeConfig[key].text;
            if (textkey != "") {
                if (textbuff[textkey]) {
                    texts = textbuff[textkey];
                } else {
                    texts = await loadTextrueDirSync("ball/" + themeConfig[key].text);
                    texts = texts.sort((a, b) => {
                        return parseInt(a.name) - parseInt(b.name);
                    });
                    textbuff[textkey] = balls;
                }
            } else {
                texts = [];
            }
            ballsThemes[key] = { balls: balls, texts: texts };
        }
        ball_wanneng = await loadTextrueSync("ball/pao_wanneng");
        ball_gift_icon = await loadTextrueSync("ball/ball_gift");
    }
    export function getBallTheme(number, key = null): { bframe, tframe } {
        if (key == null) {
            key = GameLocalStorage.gameData.themekey;
        }
        if (ballsThemes[key]) {
            return { bframe: ballsThemes[key].balls[number], tframe: ballsThemes[key].texts[number] };
        } else {
            return null;
        }
    }
    //async function loadBallThemes
    async function loadPrefabSync(file): Promise<any> {
        return new Promise((resolve, reject) => {
            cc.loader.loadRes(file, cc.Prefab, function (err, content) {
                resolve(content);
            })
        });
    }
    export async function loadTextrueSync(file): Promise<any> {
        return new Promise((resolve, reject) => {
            cc.loader.loadRes(file, cc.SpriteFrame, function (err, content) {
                if (err) {
                    resolve(null);
                } else {
                    resolve(content);
                }
            })
        });
    }
    export async function loadTextrueDirSync(dir): Promise<any> {
        return new Promise((resolve, reject) => {
            cc.loader.loadResDir(dir, cc.SpriteFrame, function (err, resource: any[], urls: string[]) {
                if (err) {
                    resolve(null);
                } else {
                    resolve(resource);
                }
            })
        });
    }
}
export module GameLocalStorage {
    export class GameData {
        currentScore: number = 0;
        bestScore: number = 0;
        gem: number = 0;//钻石
        days: number = 0;//游戏天的时间，用来判断是不是同一天
        playCount: number = 0;//总游戏次数，按照天算
        max: number = 0;//合成出来的最大值
        lastplayday: number = 0;//最后一次玩得哪天
        level: number = 1;//关卡等级
        levelprogress = 0;//关卡进度
        levelprogressscore = 0;//关卡进度分
        shotStep: number = 0;
        themekey: number = 0;
        gotThemes: number[] = [0];
        wannengprop: number = 1;
        gotPropSum: number = 0;
        inviteCount: number = 0;
        reviveTimes: number = 1;
        inrevive: string = null;//在复活中
        wannenggetcount: number = 0;
        gameSave: {
            next: {
                type: number,
                size: number
            }
            balls: {
                type: number,
                size: number,
                x: number,
                y: number
            }[]
        } = null;
        language: string = "zh";
        bianbian:number = 0;
    }
    export let gameData: GameData;
    export function Init() {
        gameData = new GameData();
        gameData.lastplayday = -1;
    }
    export function Load() {
        let str = localStorage.getItem("Po2048GameData");
        if (!!str) {
            gameData = JSON.parse(str);
            if (!gameData.inviteCount) {
                gameData.inviteCount = 0;
            }
            if (!gameData.reviveTimes) {
                gameData.reviveTimes = 1;
            }
            if (!gameData.wannenggetcount) {
                gameData.wannenggetcount = 0;
            }
            if (!gameData.language) {
                gameData.language = "zh";
            }
            if (!gameData.bianbian) {
                gameData.bianbian = 0;
            }
        }
    }
    export function Save() {
        localStorage.setItem("Po2048GameData", JSON.stringify(gameData));
    }
}
export enum GameState {
    Loading, Start, Runing, Over, SubUI
}
export module GlobleValue {
    export let gameState = GameState.Loading;
    export let examineVersion = false;
    export let reviveTimes = 1;//每局复活的次数
    export let gameSwitch: boolean = false;//判断审核开关
    export let ipSwitch: boolean = false;
}
export module AchievementData {
    export class AchievementConfigItem {
        public aID: number;
        public aType: number;
        public aName: string;
        public aNameZh: string;
        public aTip: string;
        public aTipZh: string;
        public aMaxCount: number;
        public aGem: number;
        public aweight: number = 0;
    }
    export let AchievementConfig: AchievementConfigItem[] = null;
    export let haveAchievement: number[] = [];
    export function Load() {
        if (AchievementConfig == null) {
            AchievementConfig = [
                { aID: 0, aType: 1, aName: "Novice", aTip: "Play once", aNameZh: "游戏小白", aTipZh: "天天玩泡泡", aMaxCount: 1, aGem: 10, aweight: 0 },
                { aID: 1, aType: 1, aName: "Expert", aTip: "3 consecutive days", aNameZh: "游戏高手", aTipZh: "连续玩3天", aMaxCount: 3, aGem: 20, aweight: 0 },
                { aID: 2, aType: 1, aName: "Master", aTip: "5 consecutive days", aNameZh: "游戏专家", aTipZh: "连续玩5天", aMaxCount: 5, aGem: 30, aweight: 0 },
                { aID: 3, aType: 1, aName: "Profession", aTip: "7 consecutive days", aNameZh: "游戏超人", aTipZh: "连续玩7天", aMaxCount: 7, aGem: 50, aweight: 0 },
                { aID: 4, aType: 2, aName: "2048", aTip: "Made 2048", aNameZh: "2048", aTipZh: "合成一个2048", aMaxCount: 2048, aGem: 20, aweight: 0 },
                { aID: 13, aType: 2, aName: "4096", aTip: "Made 4096", aNameZh: "4096", aTipZh: "合成一个4096", aMaxCount: 4096, aGem: 50, aweight: 0 },
                { aID: 5, aType: 3, aName: "Novice Player", aTip: "Score", aNameZh: "泡泡小白", aTipZh: "得分", aMaxCount: 10000, aGem: 20, aweight: 0 },
                { aID: 6, aType: 3, aName: "Play Expert", aTip: "Score", aNameZh: "泡泡高手", aTipZh: "得分", aMaxCount: 20000, aGem: 30, aweight: 0 },
                { aID: 7, aType: 3, aName: "Play Master", aTip: "Score", aNameZh: "泡泡达人", aTipZh: "得分", aMaxCount: 50000, aGem: 40, aweight: 0 },
                { aID: 8, aType: 3, aName: "Profession Player", aTip: "Score", aNameZh: "泡泡超人", aTipZh: "得分", aMaxCount: 100000, aGem: 50, aweight: 0 },
                { aID: 9, aType: 3, aName: "Super Player", aTip: "Score", aNameZh: "泡泡超人", aTipZh: "天天玩泡泡", aMaxCount: 200000, aGem: 100, aweight: 0 },
                //{ aID: 10, aType: 4, aName: "Collector", aTip: "获得3套皮肤", aMaxCount: 3, aGem: 10, aweight: 0 },
                { aID: 11, aType: 5, aName: "Props Novice", aTip: "Take 5 Props", aNameZh: "道具牛人", aTipZh: "拥有5个道具", aMaxCount: 5, aGem: 10, aweight: 0 },
                { aID: 12, aType: 5, aName: "Props Expert", aTip: "Take 10 Props", aNameZh: "道具专家", aTipZh: "拥有10个道具", aMaxCount: 10, aGem: 10, aweight: 0 },
                { aID: 14, aType: 5, aName: "Props Master", aTip: "Take 20 Props", aNameZh: "道具大师", aTipZh: "拥有20个道具", aMaxCount: 20, aGem: 10, aweight: 0 },
                { aID: 31, aType: 7, aName: "Cleaner", aTip: "Clean 10 feces", aNameZh: "清理工", aTipZh: "消除10个便便", aMaxCount: 10, aGem: 10, aweight: 0 },
                { aID: 32, aType: 8, aName: "Pro Cleaner", aTip: "Clean 50 feces", aNameZh: "清洁专家", aTipZh: "消除50个便便", aMaxCount: 50, aGem: 50, aweight: 0 },
                { aID: 33, aType: 9, aName: "Clean Master", aTip: "Clean 100 feces", aNameZh: "清洁大师", aTipZh: "消除100个便便", aMaxCount: 100, aGem: 100, aweight: 0 },
                /* { aID: 20, aType: 6, aName: "互相帮助", aTip: "邀请到好友", aMaxCount: 1, aGem: 10, aweight: 0 },
                { aID: 22, aType: 6, aName: "友谊第一", aTip: "邀请到好友", aMaxCount: 2, aGem: 20, aweight: 0 },
                { aID: 23, aType: 6, aName: "亲朋好友", aTip: "邀请到好友", aMaxCount: 3, aGem: 30, aweight: 0 },
                { aID: 24, aType: 6, aName: "兄弟齐心", aTip: "邀请到好友", aMaxCount: 4, aGem: 40, aweight: 0 },
                { aID: 25, aType: 6, aName: "地久天长", aTip: "邀请到好友", aMaxCount: 5, aGem: 50, aweight: 0 }, */
            ];
            AchievementConfig.sort((a, b) => {
                return b.aweight - a.aweight;
            })
        }
        let str = localStorage.getItem("Po2048AchievementData");
        if (!!str) {
            haveAchievement = JSON.parse(str);
        } else {
            haveAchievement = [];
        }
    }   
    export function Save() {
        localStorage.setItem("Po2048AchievementData", JSON.stringify(haveAchievement));
    }
}
export module Gamelanguage{
    export function getText(key){
        if(!languageData){
            languageData = {};
            languageData[ask_reset_game] = {};
            languageData[ask_reset_game].en = "Are you sure reset game?";
            languageData[ask_reset_game].zh = "确定要放弃本局吗？";
            languageData[ask_continue_game] = {};
            languageData[ask_continue_game].en = "Find the one save \n Do you want to \ncontinue playing?";
            languageData[ask_continue_game].zh = "发现游戏进度\n是否继续玩耍？";
            languageData[tip_Gemnotenough] = {};
            languageData[tip_Gemnotenough].en = "Gem is not enough.";
            languageData[tip_Gemnotenough].zh = "宝石不足！";
        }

        return languageData[key][GameLocalStorage.gameData.language];
    }
    export let ask_reset_game = "ask_reset_game";
    export let ask_continue_game = "ask_continue_game";
    export let tip_Gemnotenough = "Gemnotenough";

    let languageData;
}