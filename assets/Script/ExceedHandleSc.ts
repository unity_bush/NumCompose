import { GlobleValue, GameState } from "./ComClass";
import { Msger } from "./Msger";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class ExceedHandleSc extends cc.Component {

    start() {

    }
    private updateExceedTimer = 0;
    update(dt) {
        if (GlobleValue.gameState == GameState.Runing) {
            this.updateExceedTimer += dt;
            if (this.updateExceedTimer >= 5) {
                //Msger.emit(Msger.on_subcontext_to_exceed);
                this.updateExceedTimer = 0;
            }
        }
    }
}
