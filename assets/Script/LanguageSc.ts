import { GameLocalStorage } from "./ComClass";
import { Msger } from "./Msger";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class LanguageSc extends cc.Component {

    @property(cc.SpriteFrame)
    enSpriteFrame: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    zhSpriteFrame: cc.SpriteFrame = null;

    @property
    enText: string = '';
    @property
    zhText: string = '';

    // LIFE-CYCLE CALLBACKS:
    private static change_over = false;
    onLoad() {
        Msger.on(Msger.on_change_Language, this.changeLanguage, this);
    }

    start() {
        if (LanguageSc.change_over) {
            this.changeLanguage();
        }
    }

    // update (dt) {}
    changeLanguage() {
        LanguageSc.change_over = true;
        let label = this.getComponent(cc.Label);
        let key = GameLocalStorage.gameData.language;
        if (label) {

            let text = this[key + "Text"];
            if (text) {
                label.string = text;
            }
        }
        let sprite = this.getComponent(cc.Sprite);
        if (sprite) {
            let spriteframe = this[key + "SpriteFrame"];
            if (spriteframe) {
                sprite.spriteFrame = spriteframe;
            }
        }
    }
}
