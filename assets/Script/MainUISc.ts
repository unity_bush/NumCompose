import { Msger } from "./Msger";
import { GlobleValue, GameState, GameLocalStorage, GameData, AchievementData } from "./ComClass";
import MainSc from "./MainSc";
import BallSc from "./BallSc";
import MenuUISc from "./subUI/MenuUISc";
import MessageBoxUISc from "./subUI/MessageBoxUISc";
import TakeGemUISc from "./subUI/TakeGemUISc";


const { ccclass, property } = cc._decorator;

@ccclass
export default class MainUISc extends cc.Component {

    @property(cc.Label)
    score: cc.Label = null;
    @property(cc.Label)
    bestScore: cc.Label = null;
    @property(cc.Node)
    uiRoot: cc.Node = null;
    @property(cc.Node)
    uiBlockShareMsg: cc.Node = null;
    // 开始UI
    @property(cc.Prefab)
    uiStart: cc.Prefab = null;
    uiStartInstance: cc.Node = null;
    // 游戏结束
    @property(cc.Prefab)
    uiOver: cc.Prefab = null;
    // 菜单
    @property(cc.Prefab)
    uiMenu: cc.Prefab = null;
    private uiMenuInstance: cc.Node = null;
    // 排行
    @property(cc.Prefab)
    uiRank: cc.Prefab = null;
    // 成就
    @property(cc.Prefab)
    uiAchievement: cc.Prefab = null;
    // 道具
    @property(cc.Prefab)
    uiProp: cc.Prefab = null;
    // 皮肤
    @property(cc.Prefab)
    uiSkin: cc.Prefab = null;
    // 每日任务
    @property(cc.Prefab)
    uiDailyTask: cc.Prefab = null;
    // 每日邀请
    @property(cc.Prefab)
    uiInvite: cc.Prefab = null;
    // 复活
    @property(cc.Prefab)
    uiRevive: cc.Prefab = null;
    // 复活
    @property(cc.Prefab)
    uiMessage: cc.Prefab = null;
    // 升级
    @property(cc.Prefab)
    uiLevelUp: cc.Prefab = null;
    // 升级
    @property(cc.Prefab)
    uiHowGetGem: cc.Prefab = null;

    //获得钻石
    @property(cc.Prefab)
    uiTakeGem: cc.Prefab = null;
    //道具礼物
    @property(cc.Prefab)
    uiPropGift: cc.Prefab = null;
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.uiBlockShareMsg.active = false;
        GameLocalStorage.Init();
        Msger.on(Msger.on_ui_show_message, <any>this.on_ui_show_message, this);
        Msger.on(Msger.on_reset_score, this.on_reset_score, this);
        Msger.on(Msger.on_add_score, this.on_add_score, this);
        Msger.on(Msger.on_game_die, this.on_game_die, this);
        Msger.on(Msger.on_game_over, this.on_game_over, this);
        Msger.on(Msger.on_take_some_gem, <any>this.on_take_some_gem, this);
        Msger.on(Msger.on_ui_prop_gift, this.on_ui_prop_gift, this);
        Msger.on(Msger.on_ui_click_home, this.on_ui_click_home, this);
        Msger.on(Msger.on_ui_click_rank, this.on_ui_click_rank, this);
        Msger.on(Msger.on_ui_click_achievement, this.on_ui_click_achievement, this);
        Msger.on(Msger.on_ui_click_skin, this.on_ui_click_skin, this);
        Msger.on(Msger.on_ui_click_dailytask, this.on_ui_click_dailytask, this);
        Msger.on(Msger.on_ui_invite, this.on_ui_invite, this);
        Msger.on(Msger.on_level_up, this.on_level_up, this);
        Msger.on(Msger.on_ui_how_get_gem, this.on_ui_how_get_gem, this);
        Msger.on(Msger.on_show_block_shareui, this.on_show_block_shareui, this);
        Msger.on(Msger.on_hide_block_shareui, this.on_hide_block_shareui, this);
        this.uiRoot.addChild(this.uiMenuInstance = cc.instantiate(this.uiMenu));
        Msger.emit(Msger.on_ui_click_home);
        this.getComponent(MainSc).initGame();
        GameLocalStorage.Load();
        this.loadingData();
    }
    private async loadingData() {
        AchievementData.Load();
        await GameData.loadBallsThemes();
        Msger.emit(Msger.on_change_theme);
        Msger.emit(Msger.on_change_Language);
    }
    //start () {}

    update(dt) {
        if (GlobleValue.gameState == GameState.Runing) {
            this.score.string = GameLocalStorage.gameData.currentScore.toString();
            this.bestScore.string = GameLocalStorage.gameData.bestScore.toString();
        }
    }
    onclick_menu() {
        Msger.emit(Msger.on_ui_click_menu);
        this.uiMenuInstance.getComponent(MenuUISc).showUI();
        Msger.emit(Msger.on_sound_play, 1);
    }
    on_ui_click_home() {
        if (this.uiStartInstance) {
            this.uiRoot.addChild(this.uiStartInstance);
        } else {
            this.uiRoot.addChild(this.uiStartInstance = cc.instantiate(this.uiStart));
        }
        window.wxplatform.bannerAdHide();
        Msger.emit(Msger.on_sound_play, 1);
        GlobleValue.gameState = GameState.Start;
    }
    on_ui_click_rank() {
        this.uiRoot.addChild(cc.instantiate(this.uiRank));
        Msger.emit(Msger.on_sound_play, 1);
    }
    on_ui_click_skin() {
        this.uiRoot.addChild(cc.instantiate(this.uiSkin));
        Msger.emit(Msger.on_sound_play, 1);
    }
    on_ui_click_achievement() {
        this.uiRoot.addChild(cc.instantiate(this.uiAchievement));
        Msger.emit(Msger.on_sound_play, 1);
    }
    on_ui_click_dailytask() {
        this.uiRoot.addChild(cc.instantiate(this.uiDailyTask));
        Msger.emit(Msger.on_sound_play, 1);
    }
    on_ui_invite() {
        this.uiRoot.addChild(cc.instantiate(this.uiInvite));
        Msger.emit(Msger.on_sound_play, 1);
    }
    private on_game_die() {
        if (GameLocalStorage.gameData.reviveTimes == 1) {
            GameLocalStorage.gameData.reviveTimes -= 1;
            this.uiRoot.addChild(cc.instantiate(this.uiRevive));
            GameLocalStorage.gameData.inrevive = "1";
            GameLocalStorage.Save();
        } else {
            //显示一个死亡动画
            GlobleValue.gameState = GameState.Over;
            let ballsremove: cc.FiniteTimeAction[] = [];
            let mainSc = this.getComponent(MainSc);
            for (let ball of mainSc.ballsLayer.children) {
                if (ball == mainSc.nextball) {
                    continue;
                }
                ballsremove.push(cc.delayTime(0.1));
                ballsremove.push(cc.callFunc(this.removeBall, this, ball));
            }
            ballsremove.push(cc.callFunc(this.showOverUI, this));
            let seq = cc.sequence(ballsremove);
            this.node.runAction(seq);
        }
    }
    //一个简单的消除特效
    private removeBall(sender, ball) {
        if (ball) {
            if (!ball.parent) return;
            try {
                if (ball.getComponent(BallSc)) {
                    Msger.emit(Msger.on_sound_play_ball, ball.getComponent(BallSc).size);
                    Msger.emit(Msger.on_game_add_effect, { key: "bao", radius: BallSc.ballsRadius[ball.getComponent(BallSc).size - 1], back: true, color: BallSc.ballsColors[ball.getComponent(BallSc).size - 1], x: ball.x, y: ball.y });
                }
                ball.destroy();
            } catch (error) {

            }
        }
    }
    private showOverUI() {
        this.uiRoot.addChild(cc.instantiate(this.uiOver));
    }
    private on_add_score(score) {
        GameLocalStorage.gameData.currentScore += score;
        if (GameLocalStorage.gameData.currentScore > GameLocalStorage.gameData.bestScore) {
            GameLocalStorage.gameData.bestScore = GameLocalStorage.gameData.currentScore;
        }
    }
    private on_reset_score() {
        GameLocalStorage.gameData.currentScore = 0;
    }
    private on_game_over() {
        this.uiRoot.addChild(cc.instantiate(this.uiOver));
    }
    private on_level_up() {
        this.uiRoot.addChild(cc.instantiate(this.uiLevelUp));
        Msger.emit(Msger.on_sound_play, 2);
    }
    private on_ui_show_message(message, agreecallback, agreethisobj, closecallback = null, closethisobj = null) {
        let msgbox = cc.instantiate(this.uiMessage);
        msgbox.getComponent(MessageBoxUISc).showUI(message, agreecallback, agreethisobj, closecallback, closethisobj);
        this.uiRoot.addChild(msgbox);
        Msger.emit(Msger.on_sound_play, 1);
    }
    private on_take_some_gem(gem, fromkey, candouble = false, title = "") {
        let takegemui = cc.instantiate(this.uiTakeGem);
        takegemui.getComponent(TakeGemUISc).setGemCount(gem, fromkey, candouble, title);
        this.uiRoot.addChild(takegemui);
        Msger.emit(Msger.on_sound_play, 5);
    }
    private on_ui_prop_gift() {
        this.node.runAction(cc.sequence(cc.delayTime(0.5), cc.callFunc(() => {
            this.uiRoot.addChild(cc.instantiate(this.uiPropGift));
            Msger.emit(Msger.on_sound_play, 6);
        }, this)));
    }
    private on_ui_how_get_gem() {
        this.uiRoot.addChild(cc.instantiate(this.uiHowGetGem));
    }
    private on_show_block_shareui() {
        this.uiBlockShareMsg.active = true;
    }
    private on_hide_block_shareui() {
        this.uiBlockShareMsg.active = false;
    }
    onclick_close_ui_block_share() {
        Msger.emit(Msger.on_hide_block_shareui);
    }
}
